Copyright (C) 2012  kirill Kulakov - https://sites.google.com/site/thehighspark/home

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------

thanks to Teunis van Beelen for the 'rs232' lib, you could find him over him website teuniz.net

-----------------------------------------------

Version 0.2

HOW TO:

1. upload the sketch to the arduino
2. connect the chip (pic18f2550) to the arduino
3. run Arduino_Pic18F2550_programmer.exe
4. write the location of the .hex file (you can use the blink example)

thats it!


